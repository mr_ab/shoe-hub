# Shoe Hub - An E-commerce Platform for Footwear (B2B & B2C)

Shoe Hub is an innovative e-commerce platform designed for the footwear industry, catering to both businesses (B2B) and individual consumers (B2C). Developed as a course project for the Database Management Systems (DBMS) course, this comprehensive web application aims to provide an immersive and user-friendly experience for shoe enthusiasts and sellers alike.

## Key Features

1. **Product Catalog**: An extensive catalog showcasing a diverse range of footwear products from various brands and categories, including athletic shoes, formal shoes, sandals, and more.

2. **User Authentication**: Secure user authentication system allowing customers and sellers to create accounts, manage their profiles, and track order history.

3. **Shopping Cart**: Intuitive shopping cart functionality enabling users to add desired products, review their selections, and proceed to checkout seamlessly.

4. **Checkout and Payment Integration**: Streamlined checkout process with option for integrating desired payment gateways for secure and hassle-free transactions.

5. **Order Management**: Efficient order management system for tracking and fulfilling customer orders, including order status updates and delivery tracking.

6. **Seller Interfaces**: Dedicated interfaces for sellers to manage their product listings, inventory, and sales data.

7. **Admin Dashboard**: Comprehensive admin dashboard for managing product inventory, monitoring sales, analyzing customer and seller data, and performing administrative tasks.

## Technology Stack

- **Front-end**: Streamlit, a powerful Python web framework, was utilized to develop the user-friendly interface and facilitate seamless interactions.
- **Back-end**: PostgreSQL, a robust and scalable relational database management system, was employed to store and manage product, user, order, and seller data.
- **Database Design**: A well-structured database schema was meticulously designed to ensure efficient data storage, retrieval, and manipulation.

## Development Process

The Shoe Hub project followed an agile development methodology, with iterative cycles of requirements gathering, design, development, and testing. The team collaborated closely to ensure the successful integration of the front-end and back-end components, ensuring a cohesive and engaging user experience for both consumers and sellers.

Through its intuitive interface, secure transactions, comprehensive product catalog, and dedicated seller interfaces, Shoe Hub aims to revolutionize the footwear industry, providing customers with a seamless and enjoyable platform to discover and purchase their favorite shoes, while enabling businesses to effectively manage their product listings and sales.